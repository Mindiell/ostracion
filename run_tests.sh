#!/bin/bash

rm tests/db_test.sqlite3

ENVIRONMENT_CONFIG='.\tests\config_test.py'; nosetests --with-coverage --cover-html --cover-package=app/ --cover-erase --cover-html-dir=./tests/cover --nocapture

