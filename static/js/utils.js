var old_selected = null;
var number_of_files = 0;
var pcs = new Array();

function globalInit() {
    selectInit();
    foldersInit();
    uploadInit();
}

function selectInit() {
    context_file_menu = $id('context_file_menu');
    context_folder_menu = $id('context_folder_menu');
    lis = $("li[class^='type_']");
    lis.click(function(evt) {
        //console.log(this.value);
        selectElement(this);
        return false;
    });
    lis.dblclick(function(evt) {
        window.location = this.attributes['link'].value;
        return false;
    });
    lis = $("li[class$='file']");
    lis.bind('contextmenu', function (evt) {
        selectElement(this);
        context_file_menu.style.display = "none";
        context_folder_menu.style.display = "none";
        context_file_menu.style.display = "inline-block";
        context_file_menu.style.left = evt.clientX + 'px';
        if (evt.clientY+$("#context_file_menu").height()<window.innerHeight) {
            context_file_menu.style.top = evt.clientY-20 + 'px';
        } else {
            context_file_menu.style.top = evt.clientY-120 + 'px';
        }
        context_file_menu.aaa = element;
        document.onclick = function (evt) {
            context_file_menu.style.display = "none";
            context_folder_menu.style.display = "none";
        }
        return false;
    });
    lis = $("li[class$='folder']");
    lis.bind('contextmenu', function (evt) {
        selectElement(this);
        context_file_menu.style.display = "none";
        context_folder_menu.style.display = "none";
        context_folder_menu.style.display = "inline-block";
        context_folder_menu.style.left = evt.clientX + 'px';
        if (evt.clientY+$("#context_folder_menu").height()<window.innerHeight) {
            context_folder_menu.style.top = evt.clientY-20 + 'px';
        } else {
            context_folder_menu.style.top = evt.clientY-120 + 'px';
        }
        context_folder_menu.aaa = element;
        document.onclick = function (evt) {
            context_file_menu.style.display = "none";
            context_folder_menu.style.display = "none";
        }
        return false;
    });
}

function foldersInit() {
    elements = document.getElementsByTagName("li");
    for (i=0;i<elements.length;i++) {
        element = elements[i];
        if (element && element.className.substring(0,6)=="level_") {
            size = element.className.substring(6);
            element.style.marginLeft = (size*12) + 'px';
        }
    }
}

function uploadInit() {
    var fileselect = $id("fileselect");
    var filedrag = document.getElementsByTagName("body")[0];
    //var submitbutton = $id("submitbutton");

    // file select
    fileselect.addEventListener("change", FileSelectHandler, false);

    // is XHR2 available?
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
        // file drop
        filedrag.addEventListener("dragover", FileDragHover, false);
        filedrag.addEventListener("dragleave", FileDragLeave, false);
        filedrag.addEventListener("drop", FileSelectHandler, false);
        filedrag.style.display = "block";
        // remove submit button
        //submitbutton.style.display = "none";
    }
}

function share() {
    $("#share > #folder_name").val(old_selected.innerText);
    document.forms['share'].can_download.checked=false;
    document.forms['share'].can_upload.checked=false;
    document.forms['share'].can_delete.checked=false;
    document.forms['share'].can_rename.checked=false;
    if (old_selected.getAttribute("can_download")=="1") {
        document.forms['share'].can_download.checked=true;
    }
    if (old_selected.getAttribute("can_upload")=="1") {
        document.forms['share'].can_upload.checked=true;
    }
    if (old_selected.getAttribute("can_delete")=="1") {
        document.forms['share'].can_delete.checked=true;
    }
    if (old_selected.getAttribute("can_rename")=="1") {
        document.forms['share'].can_rename.checked=true;
    }
    show('share');
}

function rename() {
    $("#rename > #old_name").val(old_selected.innerText);
    $("#rename > #new_name").val(old_selected.innerText);
    show('rename');
    $("#rename > #new_name").select().focus();
}

function add_folder() {
    $("#add_folder > #new_name").val("");
    show('add_folder');
    $("#add_folder > #new_name").select().focus();
}

function $id(id) {
    return document.getElementById(id);
}

function FileDragHover(evt) {
    evt.stopPropagation();
    evt.preventDefault();
}

function FileDragLeave(evt) {
    evt.stopPropagation();
    evt.preventDefault();
}

function FileSelectHandler(evt) {
    // cancel event and hover styling
    FileDragHover(evt);
    // fetch FileList object
    var files = evt.target.files || evt.dataTransfer.files;
    // Init progress bar
    $(".progress_bar").css("width", "0");
    // process all File objects
    for (var i = 0, f; f = files[i]; i++) {
        //ParseFile(f);
        UploadFile(f);
    }
    number_of_files = i;
}

// upload files
function UploadFile(file) {
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
        // progress bar
        xhr.upload.addEventListener("progress", function(evt) {
            //pc = parseInt(evt.loaded / evt.total * 100);
            pcs[evt.total] = parseInt(evt.loaded / evt.total * 100);
            if (pcs[evt.total]=="100") {
                number_of_files -= 1;
                pcs[evt.total] = 0;
            }
            var pc_total = 0;
            for (pc in pcs) {
                console.log('sous-total de ' + pc + ': ' + pcs[pc]);
                pc_total += parseInt(pcs[pc]);
            }
            console.log('total: ' + pc_total);
            pc_total = parseInt(pc_total / number_of_files);
            if (isNaN(pc_total)) {
                pc_total = "100";
            }
            console.log('revenu: ' + pc_total);
            $(".progress_bar").css("width", pc_total + "%");
            $(".progress_bar").html(pc_total + "%");
            console.log(pc);
        }, false);

        // file received/failed
        xhr.onreadystatechange = function(evt) {
            if (xhr.readyState == 4) {
                $(".progress_bar").html("100%");
                $(".progress_bar").css("width", "100%");
                if (number_of_files<=0) {
                    window.location.reload(false);
                }
            }
        };

        // start upload
        if (file.size>0) {
            xhr.open("post", $id("upload").action, true);
            xhr.setRequestHeader('Content-Type', 'application/octet-stream');
            xhr.setRequestHeader("X-Filename", file.name);
            xhr.send(file);
        }
    }
}

function selectElement(element) {
    if (old_selected) {
        old_selected.style.backgroundColor = null;
    }
    element.style.backgroundColor = "#ccf";
    old_selected = element;
    //console.log(old_selected);
}

function do_action(action_name) {
    do_it = true;
    document.forms['do_action'].action.value = action_name;
    if (old_selected) {
        document.forms['do_action'].files.value = old_selected.getElementsByTagName('p')[0].innerHTML;
    }
    // TODO: Ask user if she really wants to do it ?
    /*
    if (category=='folder') {
        //do_it = confirm('Souhaitez-vous vraiment supprimer le répertoire "' + document.forms['action'].files.value + '" ?');
    } else {
        //do_it = confirm('Souhaitez-vous vraiment supprimer le fichier "' + document.forms['action'].files.value + '" ?');
    }
    */
    if (do_it) {
        document.forms['do_action'].submit();
    }
}

function do_add_folder() {
    document.forms['do_action'].new_value.value = document.forms['add_folder'].new_name.value;
    do_action('add_folder');
}

function do_admin_action(action_name, id) {
    document.forms['do_admin_action'].action.value = action_name;
    document.forms['do_admin_action'].id.value = id;
    document.forms['do_admin_action'].submit();
}

function ask_information(question, callback) {
    document.forms['question'].information_value.value = '';
    document.forms['question'].getElementsByTagName('label')[0].innerHTML = question;
    document.forms['question'].validation.onclick = callback;
    document.forms['question'].className = 'question';
    document.forms['question'].information_value.focus();
}

function cancel_information() {
    document.forms['question'].className = 'off';
    document.forms['question'].information_value = '';
}

function new_folder() {
    ask_information('Nom du nouveau répertoire: ', create_new_folder);
}

function create_new_folder() {
    document.forms['action'].files.value = document.forms['question'].information_value.value;
    document.forms['question'].className = 'off';
    document.forms['question'].information_value = '';
    do_action('create_folder');
    return false;
}

function center(id) {
    var c = $id(id);
//TODO: Finaliser centrage de la fenêtre...
/*
    console.log(c);
    console.log(c.style.width);
    console.log(c.style.height);
*/
    c.style.left = "400px";
    c.style.top = "50px";
}

function show(id) {
    $id(id).style.display = 'block';
}

function hide(id) {
    $id(id).style.display = 'none';
}

function focus(className) {
    $("#" + className).focus();
    $("." + className).focus();
}

// JQuery init function
$(function () {
    $(".connection_status").click(function(e){
        $(".connection_actions").toggle();
    });
});

