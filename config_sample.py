#encoding: utf-8

import os

APPLICATION = 'Ostracion'

DEBUG = False
HOST = '0.0.0.0'
PORT = 5000
SECRET_KEY = 'No secret key'

JINJA_ENV = {
    'TRIM_BLOCKS'   : True,
    'LSTRIP_BLOCKS' : True,
}

# defining upload folder
UPLOAD_FOLDER = os.path.abspath(os.path.dirname(__file__)) + '/upload/'

# defining base directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# defining database URI
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'db.sqlite3')
SQLALCHEMY_TRACK_MODIFICATIONS = False
