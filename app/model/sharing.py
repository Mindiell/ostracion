#encoding: utf-8

from app import admin, db
from app.model.model import Model, View

class Sharing(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    owner = db.relationship('User', backref=db.backref('share', lazy='dynamic'))
    path = db.Column(db.String(2000))
    can_download = db.Column(db.Boolean)
    can_upload = db.Column(db.Boolean)
    can_delete = db.Column(db.Boolean)
    can_rename = db.Column(db.Boolean)
    known_users = db.Column(db.Boolean)
    password = db.Column(db.String(64))
    subfolder = db.Column(db.Boolean)

# Model will be automatically managed through flask-admin module
admin.add_view(View(Sharing, db.session))
