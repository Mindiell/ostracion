#encoding: utf-8

import os
import time

import config

from app.model.sharing import Sharing

class File():
    """
    File object represents an element of the file system (a file or a folder).
    File class contains pre-formated informations.
    """
    def __init__(self, filename):
        """
        Initialize a File object based on a real file.
        """
        self.filename = filename
        self.name = os.path.basename(self.filename)
        self.path = os.path.dirname(self.filename)
        self.inner_path = config.UPLOAD_FOLDER.join(
            self.filename.split(config.UPLOAD_FOLDER)[1:]
        )
        self.is_file = os.path.isfile(self.filename)
        self.is_directory = os.path.isdir(self.filename)
        self.sharing = None
        if self.is_directory and self.name!='..':
            self.sharing = Sharing.query.filter_by(path=self.inner_path).first()
        self.last_access = time.strftime(
            "%Y-%m-%d %H:%M:%S",
            time.localtime(os.path.getatime(self.filename))
        )
        self.last_modification = time.strftime(
            "%Y-%m-%d %H:%M:%S",
            time.localtime(os.path.getmtime(self.filename))
        )
        self.size = os.path.getsize(self.filename)
        if self.size>1099511627776:
            self.human_size = "%0.2f Tb" % (self.size / 1099511627776.0)
        elif self.size>1073741824:
            self.human_size = "%0.2f Gb" % (self.size / 1073741824.0)
        elif self.size>1048576:
            self.human_size = "%0.2f Mb" % (self.size / 1048576.0)
        elif self.size>1024:
            self.human_size = "%0.2f Kb" % (self.size / 1024.0)
        else:
            self.human_size = "%0.2f b" % self.size
        if self.is_file:
            if self.filename.lower()[-3:] in ("pdf"):
                self.type = "type_pdf"
            elif self.filename.lower()[-3:] in ("png", "jpg", "svg", "peg", "gif", "iff"):
                self.type = "type_picture"
            elif self.filename.lower()[-3:] in ("avi", "mp4"):
                self.type = "type_video"
            elif self.filename.lower()[-3:] in ("wav", "ogg", "mp3"):
                self.type = "type_audio"
            else:
                self.type = 'type_generic'
        else:
            self.type = 'type_folder'

