#encoding: utf-8

from flask_admin.actions import action
import hashlib
import os

from app import admin, db
from app.model.model import Model, View
import config

def get_user(user_id):
    return User.query.get(user_id)

class User(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(200))
    password = db.Column(db.String(128))
    email = db.Column(db.String(200))
    active = db.Column(db.Boolean)
    admin = db.Column(db.Boolean)
    quota = db.Column(db.Integer, default=0)
    fill = db.Column(db.Integer, default=0)
    display = db.Column(db.Integer, default=0)

    def __init__(self, **kwargs):
        self.quota = 0
        self.fill = 0
        self.display = 0
        super().__init__(**kwargs)

    @property
    def hashed_password(self):
        return self.password

    @hashed_password.setter
    def hashed_password(self, new_password):
        self.password = hashlib.sha512(
            new_password.encode('utf-8')
        ).hexdigest()

    def check_password(self, password):
        return self.password == hashlib.sha512(
            password.encode('utf-8')
        ).hexdigest()

    @property
    def is_authenticated(self):
        return self.id is not None

    @property
    def is_active(self):
        return self.active or False

    @property
    def is_anonymous(self):
        return self.id is None

    @property
    def percentage(self):
        try:
            return (self.fill/self.quota*100.0)
        except:
            return 0

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return self.login

class UserAdmin(View):
    column_exclude_list = ['password', ]

    def on_model_change(self, form, model, is_created):
        if len(form.password.data)<128:
            model.hashed_password = form.password.data

    def create_model(self, model):
        # When creating a user, we should create its main folder too
        folder = os.path.join(config.UPLOAD_FOLDER, model.login.data)
        os.mkdir(folder)
        return super().create_model(model)

    def delete_model(self, model):
        # When deleting a user, we should delete its main folder too
        folder = os.path.join(config.UPLOAD_FOLDER, model.login)
        for root, dirs, files in os.walk(folder, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        os.rmdir(folder)
        super().delete_model(model)

    @action('refresh', 'Refresh')
    def action_refresh(self, ids):
        users = User.query.filter(User.id.in_(ids)).all()
        for user in users:
            folder = os.path.join(config.UPLOAD_FOLDER, user.login)
            user.fill = get_tree_size(folder)
            user.save()

def get_tree_size(path):
    total = 0
    for entry in os.scandir(path):
        if entry.is_dir(follow_symlinks=False):
            total += get_tree_size(entry.path)
        else:
            total += entry.stat(follow_symlinks=False).st_size
    return total

# Model will be automatically managed through flask-admin module
admin.add_view(UserAdmin(User, db.session))
