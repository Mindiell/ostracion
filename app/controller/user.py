#encoding: utf-8

from flask import render_template, g, url_for, redirect, flash, session, request
from flask_login import login_user, logout_user, current_user

from app.controller.controller import Controller
from app.form.user import UserLoginForm, UserNewPasswordForm, UserOptionsForm
from app.model.sharing import Sharing as SharingModel
from app.model.user import User as UserModel

class User(Controller):
    def login(self):
        g.form = UserLoginForm()
        if g.form.validate_on_submit():
            user = UserModel.query.filter_by(login=g.form.login.data).first()
            if user and user.check_password(g.form.password.data):
                if user.is_active:
                    login_user(user)
                    if user.display==1:
                        session["display"] = "details"
                    else:
                        session["display"] = "icons"
                    return redirect(url_for('file.browse', path=user.login))
                else:
                    flash("Compte inactif", "warning")
            else:
                flash("Mauvais identifiant ou mot de passe.", "warning")
        for (field, message) in g.form.errors.items():
            flash('%s: %s' % (field, message[0]))
        return render_template('user/login.html')

    def logout(self):
        logout_user()
        session.clear()
        return redirect(url_for('core.home'))

    def options(self):
        g.owner = current_user
        g.password_form = UserNewPasswordForm()
        g.form = UserOptionsForm(obj=current_user)
        if g.form.validate_on_submit():
            if g.form["display"].data and current_user.display==0:
                current_user.display = 1
                current_user.save()
            elif not g.form["display"].data and current_user.display>0:
                current_user.display = 0
                current_user.save()
        return render_template('user/options.html')

    def password(self):
        g.form = UserNewPasswordForm()
        if g.form.validate_on_submit():
            if g.form["password"].data!=g.form["confirm_password"].data:
                flash("Le mot de passe de confirmation n'est pas identique au mot de passe", "danger")
            else:
                current_user.hashed_password = g.form["password"].data
                current_user.save()
        return redirect(url_for('user.options'))

    def sharing(self):
        g.owner = current_user
        g.sharings = SharingModel.query.filter_by(owner_id=g.owner.id).all()
        return render_template('user/sharing.html')
