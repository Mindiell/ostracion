#encoding: utf-8

from flask import render_template, redirect, url_for, g, flash, request, send_file, session
from flask_login import current_user
import html
import os

import config

from app.controller.controller import Controller
from app.model.file import File as FileModel
from app.model.sharing import Sharing as SharingModel
from app.model.user import User as UserModel

class File(Controller):
    def get_files(self, path, parent=False):
        """
        Retrieve and returns the list of all folders and files (first level) for a specific path.
        the 'parent' argument sets if the '..' folder is to add or not to the list.
        """
        realpath = os.path.realpath(path)
        if not os.path.isdir(realpath):
            # TODO: This path is not existing, what to do ???
            return []
        folders = sorted([
                    FileModel(os.path.join(realpath, folder_name)) for folder_name 
                    in os.listdir(realpath)
                    if os.path.isdir(os.path.join(realpath, folder_name))
                ], key=lambda k: k.name)
        if parent:
            folders.insert(0, FileModel(os.path.join(realpath, '..')))
        files = sorted([
                    FileModel(os.path.join(realpath, folder_name)) for folder_name 
                    in os.listdir(realpath)
                    if os.path.isfile(os.path.join(realpath, folder_name))
                ], key=lambda k: k.name)
        folders.extend(files)
        return folders

    def browse(self, path=""):
        if path[-1]=="/":
            path = path[:-1]
        current_path = os.path.join(config.UPLOAD_FOLDER, path)
        # Look for specific sharing
        g.path = path
        g.sharing = SharingModel.query.filter_by(
            path=g.path
        ).first()
        # Looking for owner
        g.owner = UserModel.query.filter_by(login=path.split("/")[0]).first()
        if current_user.is_anonymous or g.owner.login!=current_user.login:
            # Not owner, so if there is no sharing for this path, user should be rejected
            if g.sharing is None:
                return redirect(url_for('core.home'))
        else:
            # Owner should have all rights
            g.sharing = SharingModel(
                owner_id=current_user.id,
                path=g.path,
                can_download = True,
                can_upload = True,
                can_delete = True,
                can_rename = True,
            )
        g.breadcrumbs = [(path.split('/')[key], os.path.sep.join((path.split('/')[:key+1]))) for key in range(len(path.split('/')))]
        g.filelist = self.get_files(current_path, current_user.is_authenticated and len(g.breadcrumbs)>1)
        try:
            g.display = session["display"]
        except:
            g.display = "icons"
        return render_template('file/browse.html')

    def upload(self, path):
        current_path = os.path.join(config.UPLOAD_FOLDER, path)
        owner = UserModel.query.filter_by(login=path.split("/")[0]).first()
        if owner is not None:
            if 'X-filename' in request.headers:
                if owner.fill+len(request.data)>owner.quota:
                    flash("Sorry but this file seems too large for actual quota.", "danger")
                else:
                    os.makedirs(os.path.join(current_path), exist_ok=True)
                    with open(os.path.join(current_path, request.headers['X-filename']), 'wb') as f:
                        f.write(request.data)
                    # Updating fill
                    owner.fill += len(request.data)
                    owner.save()
                    flash("File successfully added!", "success")
        return ""

    def action(self, path):
        current_path = os.path.join(config.UPLOAD_FOLDER, path)
        if request.form["action"]=="delete":
            size = 0
            try:
                filepath = os.path.sep.join((current_path, request.form["files"]))
                size = os.path.getsize(filepath)
                os.remove(filepath)
                flash("Fichier supprimé", "success")
            except Exception as exception:
                if exception.errno==21:
                    size = 0
                    # It's a folder
                    folder = os.path.sep.join((current_path, request.form["files"]))
                    for root, dirs, files in os.walk(folder, topdown=False):
                        for name in files:
                            size += os.path.getsize(os.path.join(root, name))
                            os.remove(os.path.join(root, name))
                        for name in dirs:
                            os.rmdir(os.path.join(root, name))
                    my_file = FileModel(folder)
                    if my_file.sharing is not None:
                        my_file.sharing.delete()
                    os.rmdir(folder)
                    flash("Folder successfully deleted!", "success")
                elif exception.errno==2:
                    # File not found, maybe encoded name ?
                    filepath = os.path.sep.join((current_path, html.unescape(request.form["files"])))
                    size = os.path.getsize(filepath)
                    os.remove(filepath)
                    flash("File successfully deleted!", "success")
                else:
                    print(exception.errno, exception)
            # Updating fill
            owner = UserModel.query.filter_by(login=path.split("/")[0]).first()
            owner.fill -= size
            owner.save()
        elif request.form["action"]=="download":
            try:
                return send_file(
                    os.path.sep.join((current_path, request.form["files"])),
                    as_attachment=True
                )
            except Exception as exception:
                if exception.errno==21:
                    # It's a folder
                    #TODO: Should compress folder and send result file
                    pass
                else:
                    # Other exception, let's display an alert
                    flash("File seems not available", "danger")
        elif request.form["action"]=="details":
            session["display"] = "details"
        elif request.form["action"]=="icons":
            session["display"] = "icons"
        elif request.form["action"]=="add_folder":
            folder_name = request.form["new_value"]
            folder = os.path.sep.join((current_path, folder_name))
            if os.path.exists(folder):
                flash("Folder already exists!", "danger")
            else:
                os.mkdir(folder)
                flash("Folder successfully created!", "success")
        elif request.form["action"]=="rename":
            my_file = FileModel(os.path.sep.join((current_path, request.form["old_name"])))
            if my_file.sharing is not None:
                my_file.sharing.path = os.path.sep.join((path, request.form["new_name"]))
                my_file.sharing.save()
            src = os.path.sep.join((current_path, request.form["old_name"]))
            dst = os.path.sep.join((current_path, request.form["new_name"]))
            os.rename(src, dst)
            flash("File successfully renamed!", "success")
        elif request.form["action"]=="share":
            my_file = FileModel(os.path.sep.join((current_path, request.form["folder_name"])))
            if my_file.sharing is not None:
                my_file.sharing.delete()
            if request.form.get("can_download",0) or request.form.get("can_upload",0) or request.form.get("can_delete",0) or request.form.get("can_rename",0):
                SharingModel(
                    owner_id=current_user.id,
                    path=my_file.inner_path,
                    known_users=0,
                    can_download=int(request.form.get("can_download",0)),
                    can_upload=int(request.form.get("can_upload",0)),
                    can_delete=int(request.form.get("can_delete",0)),
                    can_rename=int(request.form.get("can_rename",0)),
                ).save()
                flash("Folder successfully shared!", "success")
            else:
                flash("Sharing successfully deleted!", "success")            
        return redirect(url_for('file.browse', path=path))

    def add_folder(self, path=""):
        if path[-1]=="/":
            path = path[:-1]
        g.path = path
        return render_template('file/add_folder.html')
