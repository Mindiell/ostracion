#encoding: utf-8

from flask import render_template, redirect, url_for, g
from flask_login import current_user

from app.controller.controller import Controller
from app.model.user import User as UserModel

class Core(Controller):
    def home(self):
        if current_user.is_authenticated:
            return redirect(url_for("file.browse", path=current_user.login))
        g.owner = UserModel()
        return render_template('core/home.html')

