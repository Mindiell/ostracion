#encoding: utf-8

from app.controller.admin import Admin
from app.controller.core import Core
from app.controller.file import File
from app.controller.user import User

routes = [
    ('/', Core.as_view('home')),

    ('/login', User.as_view('login'), ['GET', 'POST']),
    ('/logout', User.as_view('logout')),
    ('/options', User.as_view('options'), ['GET', 'POST']),
    ('/sharing', User.as_view('sharing'), ['GET', 'POST']),
    ('/password', User.as_view('password'), ['POST']),

    ('/b/<path:path>', File.as_view('browse')),
    ('/upload/<path:path>', File.as_view('upload'), ['POST']),
    ('/action/<path:path>', File.as_view('action'), ['POST']),

    ('/admin/login', Admin.as_view('login'), ['GET', 'POST']),
]

apis = [
]
