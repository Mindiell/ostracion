del tests\db_test.sqlite3

set ENVIRONMENT_CONFIG=.\tests\config_test.py

nosetests --with-coverage --cover-html --cover-package=. --cover-erase --cover-html-dir=./tests/cover --nocapture

