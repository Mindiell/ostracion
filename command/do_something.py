#encoding: utf-8

import click
from flask.cli import with_appcontext

@click.command(help="This command is doing something")
@with_appcontext
def do_something():
    click.echo("do something")

