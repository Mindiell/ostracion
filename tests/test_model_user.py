#encoding: utf-8

from setup import Setup

from app.model.user import User

class TestModelUser(Setup):
    def test_creation(self):
        users = User.query.all()
        self.assertEqual(len(users), 4)
        user = User(login='test', password='test', email='', active=True, admin=True)
        user.save()
        users = User.query.all()
        self.assertEqual(len(users), 5)

    def test_anonymous_user(self):
        user = User()
        self.assertEqual(user.is_anonymous, True)
        user = User(login='test', password='test', email='', active=True, admin=True)
        user.save()
        self.assertEqual(user.is_anonymous, False)
