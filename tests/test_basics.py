#encoding: utf-8

from setup import Setup

class TestBasic(Setup):
    def test_loading_homepage(self):
        result = self.client.get('/')
        self.assertIn('Homepage', str(result.data))
