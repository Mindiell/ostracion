#encoding: utf-8

from flask import session
from flask_login import current_user

from setup import Setup

class TestAdmin(Setup):
    def test_loading_admin_false_homepage(self):
        result = self.client.get('/admin', follow_redirects=True)
        self.assertIn('Login', str(result.data))

    def test_admin_logging_normally(self):
        result = self.client.post('/admin/login', data={'login':'admin', 'password':'test'}, follow_redirects=True)
        self.assertIn('User', str(result.data))
        with self.client.session_transaction() as my_session:
            self.assertEqual('1', my_session['user_id'])

    def test_admin_logging_normally_then_logout(self):
        result = self.client.post('/admin/login', data={'login':'admin', 'password':'test'}, follow_redirects=True)
        result = self.client.get('/admin/logout', follow_redirects=True)
        self.assertIn('Login', str(result.data))

    def test_admin_logging_without_login(self):
        result = self.client.post('/admin/login', data={'login':'', 'password':'test'}, follow_redirects=True)
        self.assertIn('login: This field is required.', str(result.data))

    def test_admin_logging_without_password(self):
        result = self.client.post('/admin/login', data={'login':'admin', '':'test'}, follow_redirects=True)
        self.assertIn('password: This field is required.', str(result.data))

    def test_logging_active(self):
        result = self.client.post('/admin/login', data={'login':'active', 'password':'test'}, follow_redirects=True)
        self.assertNotIn('User', str(result.data))
        self.assertIn('Back to website', str(result.data))

    def test_inaccessible(self):
        result = self.client.post('/admin/login', data={'login':'active', 'password':'test'}, follow_redirects=True)
        result = self.client.get('/admin/user/', follow_redirects=True)
        self.assertNotIn('User', str(result.data))
        self.assertIn('Back to website', str(result.data))

    def test_admin_logging_inactive(self):
        result = self.client.post('/admin/login', data={'login':'inactive', 'password':'test'}, follow_redirects=True)
        self.assertIn('Compte inactif', str(result.data))

    def test_admin_list_users(self):
        # Login
        self.client.post('/admin/login', data={'login':'admin', 'password':'test'}, follow_redirects=True)

        result = self.client.get('/admin/user/', follow_redirects=True)
        self.assertIn('foo', str(result.data))
        self.assertIn('foo@bar.com', str(result.data))

    def test_admin_edit_user(self):
        # Login
        self.client.post('/admin/login', data={'login':'admin', 'password':'test'}, follow_redirects=True)

        result = self.client.get('/admin/user/edit/?url=%2Fadmin%2Fplayer%2F&id=4', follow_redirects=True)
        self.assertIn('foo', str(result.data))
        # FIXME: On ne devrait pas afficher le mot de passe, même hashé, de l'utilisateur
        self.assertIn('bar', str(result.data))
        self.assertIn('foo@bar.com', str(result.data))

    def test_wrong_auth(self):
        # Login
        result = self.client.post('/admin/login', data={'login':'foo', 'password':'foobar'}, follow_redirects=True)
        self.assertIn('Login', str(result.data))
