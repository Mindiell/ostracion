#encoding: utf-8

import os

APPLICATION = 'Ostracion'

DEBUG = False
TESTING = True
HOST = '0.0.0.0'
PORT = 5005
SECRET_KEY = 'Choose a secret key'

JINJA_ENV = {
    'TRIM_BLOCKS'   : True,
    'LSTRIP_BLOCKS' : True,
}

# defining base directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# defining database URI
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'db_test.sqlite3')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Disable CSRF for tests
WTF_CSRF_ENABLED = False
