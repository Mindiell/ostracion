#encoding: utf-8

import unittest

import server

class Setup(unittest.TestCase):
    def setUp(self):
        # Initialize application for testing
        self.app = server.app
        self.app.config.from_object('config_test')
        self.login_manager = server.login_manager

        self._ctx = self.app.test_request_context()
        self._ctx.push()

        # Initialize Database
        from app import db
        self.db = db
        self.db.create_all()
        self.populate()

        self.client = self.app.test_client()

    def tearDown(self):
        # Remove Database
        self.db.session.remove()

        # Close session
        with self.client.session_transaction() as my_session:
            my_session.clear()

        # Removing context
        if self._ctx is not None:
            self._ctx.pop()

    def populate(self):
        # Creating users
        self.db.engine.execute("delete from user")
        self.db.engine.execute("insert into user values (1, 'admin', '$2b$16$UPcqNCOkV2FjMXI2A4abtumNY3b144zHvI0bNO3DfZm3EePc4xCi2', '', 1, 1, 0, 0, 0)")
        self.db.engine.execute("insert into user values (2, 'active', '$2b$16$UPcqNCOkV2FjMXI2A4abtumNY3b144zHvI0bNO3DfZm3EePc4xCi2', '', 1, 0, 1024, 0, 0)")
        self.db.engine.execute("insert into user values (3, 'inactive', '$2b$16$UPcqNCOkV2FjMXI2A4abtumNY3b144zHvI0bNO3DfZm3EePc4xCi2', '', 0, 0, 1024, 0, 0)")
        self.db.engine.execute("insert into user values (4, 'foo', '$2b$16$ryP.FocM60LHHhsUbDQl3.QciWlgnFbELxlv7LiO9dZ7LK9aJDxRK', 'foo@bar.com', 0, 0, 1024, 0, 0)")

        # Creating sharing
        self.db.engine.execute("delete from sharing")
        self.db.engine.execute("insert into sharing values (1, 2, 'active/test', 0, 0, 0, 0, 0, '', 0)")
